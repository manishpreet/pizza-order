package com.manishpreet.mompopspizza.utils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

class Preference (context: Context) {
    val PREFS_FILENAME = "com.preferences"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, MODE_PRIVATE)
}