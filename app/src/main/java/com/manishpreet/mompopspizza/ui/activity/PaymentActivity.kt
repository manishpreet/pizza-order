package com.manishpreet.mompopspizza.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.Order
import com.manishpreet.mompopspizza.data.RealCart
import com.manishpreet.mompopspizza.data.User
import com.manishpreet.mompopspizza.networking.FirestoreHelper
import com.manishpreet.mompopspizza.ui.HomeActivity
import com.manishpreet.mompopspizza.ui.SignInActivity
import com.manishpreet.mompopspizza.ui.adapter.PaymentAdapter
import com.manishpreet.mompopspizza.ui.adapter.getTotalPrice
import com.manishpreet.mompopspizza.ui.dialog.PaymentDialog
import com.manishpreet.mompopspizza.ui.dialog.SuccessDialog
import com.manishpreet.mompopspizza.utils.*
import com.sachtechsolution.friendfinder.extension.clearRealm
import kotlinx.android.synthetic.main.activity_payment.*

class PaymentActivity:AppCompatActivity(),getTotalPrice
{
    val firestoreHelper by lazy { FirestoreHelper.getInstance() }
    val user by lazy { getprefObject<User>("user") }
    override fun getTotal(total: Int) {
        payment_subtotal.text= "Subtotal : $total"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        val list=Constants.CARTLIST
        payment_count.text= "Items : ${list?.size.toString()}"
        payment_subtotal.text="Subtotal: ${gettotal(list)}"
        payment_recycler.adapter=PaymentAdapter(list)
        back_button.onClick {
            onBackPressed()
        }
        payment_place.onClick {
            showProgress()
          var order = Order(
              order_number= generateRandomString(10),
              sub_total = payment_subtotal.text.toString(),
              uid = user.uid,
              orders = (list as ArrayList<RealCart>?)

          )
            firestoreHelper.placeOrder(order)
                .addOnCompleteListener {
                    if (it.isSuccessful)
                    {
                        toast("Order Placed")
                        hideProgress()
                        clearRealm()
                        SuccessDialog().show(supportFragmentManager, PaymentDialog::class.java.name)

                    }

                }.addOnFailureListener {
                    hideProgress()
                    toast(it?.message!!)
                }
        }
    }

    fun generateRandomString(len: Int = 15): String{
        val alphanumerics = CharArray(26) { it -> (it + 97).toChar() }.toSet()
            .union(CharArray(9) { it -> (it + 48).toChar() }.toSet())
        return (0..len-1).map {
            alphanumerics.toList().random()
        }.joinToString("")
    }
    private fun gettotal(list: MutableList<RealCart>?): Int {
        var price=0
        for (i in 0 until list?.size!!) {
            price += list?.get(i)?.price!!.toInt()
        }
        return price
    }

}