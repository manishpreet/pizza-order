package com.manishpreet.mompopspizza.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.Pizza_
import com.manishpreet.mompopspizza.ui.dialog.AddItemOptionDialog
import com.manishpreet.mompopspizza.ui.activity.ProductActivity
import com.manishpreet.mompopspizza.utils.inflate
import com.manishpreet.mompopspizza.utils.onClick
import com.manishpreet.mompopspizza.utils.toast
import com.sachtechsolution.friendfinder.extension.saveCard
import com.sachtechsolution.friendfinder.ui.home.contacts.model.Card
import kotlinx.android.synthetic.main.item_product.view.*

class PizzaAdapter(
    val list: List<Pizza_>?,
    val context: ProductActivity
) : RecyclerView.Adapter<PizzaAdapter.MyPizzaAdapter>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyPizzaAdapter {
        val view = p0?.inflate(R.layout.item_product)
        return MyPizzaAdapter(view)
    }

    override fun onBindViewHolder(holder: MyPizzaAdapter, i: Int) {
val pizza=list?.get(i)
        holder.itemView.product_name.text= pizza?.name
        holder.itemView.product_price.text="Price : $${pizza?.price}"
        val url="${pizza?.baseUrl}${pizza?.image}"
        Glide.with(context).asBitmap().load(url).into(holder.itemView.product_image)
        holder.itemView.product_add.also {

            it.onClick {
                if (it.text.toString().equals("Add "))
                {
                    var card = Card(
                        name = pizza?.name,
                        order_number = pizza?.id,
                        price = pizza?.price,
                        count = 1,
                        note = pizza?.type
                    )
                    saveCard(card)
                    toast("added to cart")
                    holder.itemView.product_add.text = "Added"
                }

            }

        }
    }

    override fun getItemCount(): Int {
        return list?.size!!
    }

    inner class MyPizzaAdapter(view: View) : RecyclerView.ViewHolder(view)
}
