package com.manishpreet.mompopspizza.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.utils.Constants
import com.manishpreet.mompopspizza.utils.addFragment
import com.manishpreet.mompopspizza.utils.replaceFragment

class SignInActivity: AppCompatActivity(),HomeInteractor{
    private val container=R.id.signin_container
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        addFragment(SignInFragment())
    }


    override fun replaceFragment(fragment: Fragment) {
        replaceFragment(container,fragment)
    }

    override fun addFragment(fragment: Fragment) {
        addFragment(container,fragment)
    }
}
