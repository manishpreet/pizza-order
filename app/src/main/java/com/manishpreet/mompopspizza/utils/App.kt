package com.manishpreet.mompopspizza.utils

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import io.realm.Realm
import io.realm.RealmConfiguration

class App:Application()
{
    override fun onCreate() {
        super.onCreate()
        application =this
        preferences = Preference(applicationContext).prefs
        preferencesEditor= preferences?.edit()
        initRealm()
    }
    companion object {
        var preferences: SharedPreferences? = null
        var preferencesEditor: SharedPreferences.Editor? = null
        var application:Context?=null
    }

    private fun initRealm() {
        Realm.init(this)
        val config = RealmConfiguration.Builder()
            .name("prontoschool.realm")
            .schemaVersion(1)
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(config)

    }
}