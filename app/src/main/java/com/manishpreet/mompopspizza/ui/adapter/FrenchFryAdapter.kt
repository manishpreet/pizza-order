package com.manishpreet.mompopspizza.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.FrenchFry
import com.manishpreet.mompopspizza.ui.dialog.AddItemOptionDialog
import com.manishpreet.mompopspizza.ui.activity.ProductActivity
import com.manishpreet.mompopspizza.utils.inflate
import com.manishpreet.mompopspizza.utils.onClick
import com.manishpreet.mompopspizza.utils.toast
import com.sachtechsolution.friendfinder.extension.saveCard
import com.sachtechsolution.friendfinder.ui.home.contacts.model.Card
import kotlinx.android.synthetic.main.item_product.view.*

class FrenchFryAdapter(
    val list: List<FrenchFry>?,
    val context: ProductActivity
) : RecyclerView.Adapter<FrenchFryAdapter.MyFrenchFryAdapter>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyFrenchFryAdapter {
        return MyFrenchFryAdapter(p0.inflate(R.layout.item_product))
    }

    override fun getItemCount(): Int {
return list?.size!!
    }

    override fun onBindViewHolder(holder: MyFrenchFryAdapter, i: Int) {
        val frenchFry=list?.get(i)
        holder.itemView.product_name.text= frenchFry?.name
        holder.itemView.product_price.text="Price : $${frenchFry?.price}"
        val url="${frenchFry?.baseUrl}${frenchFry?.image}"
        Glide.with(context).asBitmap().load(url).into(holder.itemView.product_image)
        holder.itemView.product_add.also {

            it.onClick {
                if (it.text.toString().equals("Add "))
                {
                    var card = Card(
                        name = frenchFry?.name,
                        order_number = frenchFry?.id,
                        price = frenchFry?.price,
                        count = 1,
                        note = frenchFry?.type
                    )
                    saveCard(card)
                    toast("added to cart")
                    holder.itemView.product_add.text = "Added"
                }

            }

        }
    }

    inner class MyFrenchFryAdapter(view: View): RecyclerView.ViewHolder(view)
}
