package com.manishpreet.mompopspizza.networking


import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.storage.FirebaseStorage
import com.manishpreet.mompopspizza.data.Order
import com.manishpreet.mompopspizza.data.User

class FirestoreHelper
{
    companion object {
        lateinit var firestore : FirebaseFirestore
        lateinit var firebaseStorage: FirebaseStorage

       fun getInstance(): FirestoreHelper {

           firestore = FirebaseFirestore.getInstance()
           firebaseStorage= FirebaseStorage.getInstance()
           val instance by lazy { FirestoreHelper() }
           return instance
        }
    }
    fun getUser(uid : String)= firestore.collection("users").document(uid).get()
    fun saveUser( uid : String,user : User)=firestore.collection("users").document(uid).set(user)
    fun addItems(product:String,itemuniqueid:String)= firestore.collection("orders").document(itemuniqueid).set(product)
    fun changeProfilePic(uid: String,map: Map<String,String>)= firestore.collection("users").document(uid).update(map)
    fun placeOrder(order : Order)= firestore.collection("orders").document().set(order)
    fun getPlacedOrder(uid: String)=firestore.collection("orders").whereEqualTo("uid",uid).orderBy("timestemp",Query.Direction.DESCENDING).get()
}