package com.manishpreet.mompopspizza.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.EditText
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.utils.*
import kotlinx.android.synthetic.main.activity_location.*

class LocationActivity : AppCompatActivity() {

    var places: SupportPlaceAutocompleteFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)
        getLocation()
        setOnClick()
    }

    private fun setOnClick() {
        btn_next.onClick {
            launchActivity<HomeActivity>()
            finish()
        }
        txtSkip.onClick {
            launchActivity<HomeActivity>()
            finish()
        }
    }

    private fun getLocation() {
        places = supportFragmentManager.findFragmentById(R.id.place_autocomplete) as SupportPlaceAutocompleteFragment
        val typeFilter = AutocompleteFilter.Builder()
            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).setCountry("IN")
            .build()
        places?.setFilter(typeFilter)
        (places?.view?.findViewById(R.id.place_autocomplete_search_input) as? EditText)?.textSize = 16.0f
        places?.setOnPlaceSelectedListener(object : PlaceSelectionListener {

            override fun onPlaceSelected(place: Place) {
                set("place", "${place.name},${place.address}")
                btn_next.visibility = View.VISIBLE
            }

            override fun onError(status: Status) {
                Log.e("exception", status.statusMessage)
            }
        })
    }
}