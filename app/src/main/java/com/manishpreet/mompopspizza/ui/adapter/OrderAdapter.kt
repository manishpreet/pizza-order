package com.manishpreet.mompopspizza.ui

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.Order
import com.manishpreet.mompopspizza.utils.inflate
import kotlinx.android.synthetic.main.item_order.view.*
import java.text.SimpleDateFormat
import java.util.*
import android.text.format.DateFormat
import com.google.gson.Gson
import com.manishpreet.mompopspizza.ui.activity.OrderDetailActivty
import com.manishpreet.mompopspizza.utils.launchActivity
import com.manishpreet.mompopspizza.utils.onClick


class OrderAdapter: RecyclerView.Adapter<OrderAdapter.MyOrderAdapter>()
{
    var list= ArrayList<Order>()
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyOrderAdapter {
        val view=p0.inflate(R.layout.item_order)
        return MyOrderAdapter(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyOrderAdapter, position: Int) {

        var data=list.get(position)
        holder.itemView.apply {
            var price=data.sub_total?.split(":")
            item_order_date.text=parseDate(data.timestemp!!)
            item_order_number.text="order no : ${data.order_number}"
            item_order_price.text="$ ${price!![1]}"
            view_order.onClick {
                var order=Gson().toJson(list.get(position))
                context.launchActivity<OrderDetailActivty>()
                {
                    putExtra("order",order)
                }
            }

        }
    }
    private fun parseDate(time:Long): String {

        // or you already have long value of date, use this instead of milliseconds variable.
        val dateString = DateFormat.format("d MMM ,yyyy", Date(time)).toString()
        return dateString
    }

    fun add(order: Order) {
        list.add(order)
        notifyDataSetChanged()

    }

    inner class MyOrderAdapter(view:View): RecyclerView.ViewHolder(view)
}