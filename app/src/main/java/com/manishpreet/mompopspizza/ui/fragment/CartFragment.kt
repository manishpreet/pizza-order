package com.manishpreet.mompopspizza.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.RealCart
import com.manishpreet.mompopspizza.ui.activity.PaymentActivity
import com.manishpreet.mompopspizza.ui.adapter.CartAdapter
import com.manishpreet.mompopspizza.ui.dialog.PaymentDialog
import com.manishpreet.mompopspizza.utils.*
import com.sachtechsolution.friendfinder.extension.getCards
import kotlinx.android.synthetic.main.fragment_cart.*

class CartFragment : Fragment() {
     var adapter : CartAdapter ?=null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_cart)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cart_pay.onClick { PaymentDialog().show(fragmentManager, CartFragment::class.simpleName) }
        var cardList = getCards()

        if (cardList?.size!! > 0) {
            var list =ArrayList<RealCart>()
            for ( i in 0 until cardList.size)
            {
                var cart=RealCart(name = cardList.get(i).name,
                    price = cardList.get(i).price,
                    count = cardList.get(i).count,
                    order_number = cardList.get(i).order_number,
                    note = cardList.get(i).note
                    )
                list.add(cart)
            }
            cart_pay.visibility = View.VISIBLE
            adapter=CartAdapter(list,cardList)
            cart_recycler.adapter = adapter
            cart_emptyView.gone()
        } else {
            cart_emptyView.visible()
            cart_pay.visibility = View.GONE
        }

      cart_pay.onClick {
          Constants.CARTLIST=adapter?.cardList
          context?.launchActivity<PaymentActivity>()
      }
    }

}


