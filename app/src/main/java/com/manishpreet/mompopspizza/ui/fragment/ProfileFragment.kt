package com.manishpreet.mompopspizza.ui.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.User
import com.manishpreet.mompopspizza.networking.FirestoreHelper
import com.manishpreet.mompopspizza.ui.LocationActivity
import com.manishpreet.mompopspizza.ui.SignInActivity
import com.manishpreet.mompopspizza.utils.*
import com.sachtechsolution.friendfinder.extension.clearRealm
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.gallery_dialog.*
import java.io.ByteArrayOutputStream


class ProfileFragment : Fragment() {

    var dialog: Dialog? = null
    val CAMERA_REQUEST_CODE = 10
    val GALLERY_REQUEST_CODE = 20
    var path: String? = null
    val firestore by lazy { FirestoreHelper.getInstance() }
    var uri: Uri? = null
    val user by lazy { getprefObject<User>("user") }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_profile)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val address = get("place", "")
        profile_address.text = address
        profile_name.text = user.name
        profile_email.text = user.email
        profile_phone.text = user.mobile
        Glide.with(context!!).asBitmap().load(user.image).into(profile_image)
        profile_edit_address.onClick {
            context?.startActivity(
                Intent(
                    context,
                    LocationActivity::class.java
                )
            )
        }

        profile_choose.onClick {
            selectImageFromChooser()
        }

        profile_logout.onClick {
            clearAll()
            clearRealm()
            val intent = Intent(context!!, SignInActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context?.startActivity(intent)
        }
    }

    private fun selectImageFromChooser() {
        dialog = Dialog(context!!)
        dialog?.setContentView(R.layout.gallery_dialog)
        dialog?.dialog_text_camera!!.setOnClickListener { selectImageFromCamera() }
        dialog?.dialog_text_gallery!!.setOnClickListener { selectImageFromGallery() }
        dialog?.show()
    }

    private fun selectImageFromGallery() {
        if (checkPermissions(
                arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                )
            )
        )
            openGallery()
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity?.requestPermissions(
                    arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                    GALLERY_REQUEST_CODE
                )
            }
        }
    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    private fun selectImageFromCamera() {
        if (checkPermissions(
                arrayOf(
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                )
            )
        )
            openCamera()
        else
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity?.requestPermissions(
                    arrayOf(
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ), CAMERA_REQUEST_CODE
                )
            }
    }

    private fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }

    private fun getImageUri(context: Context, photo: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(context.contentResolver, photo, "Title", null)
        return Uri.parse(path)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_REQUEST_CODE)
            selectImageFromCamera()
        else if (requestCode == GALLERY_REQUEST_CODE)
            selectImageFromGallery()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                val bitmap = data?.extras?.get("data") as Bitmap
                uri = getImageUri(context!!, bitmap)
                path = FileUtils.getPath(context!!, uri!!)

            } else if (requestCode == GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                uri = data?.data
                path = FileUtils.getPath(context!!, uri!!)
            }
            uploadImage()

        }
        if (dialog != null) {
            dialog?.dismiss()
        }
    }

    fun uploadImage() {
context?.showProgress()
        val storage = FirebaseStorage.getInstance()
        val storageref = storage.getReferenceFromUrl("gs://momspops-4b058.appspot.com/")
        val reference = storageref.child("${uri?.lastPathSegment}")
        val uploadtask = reference.putFile(uri!!)
        uploadtask.addOnSuccessListener {
            val task = it?.metadata?.reference?.downloadUrl?.addOnSuccessListener {
                val taskurl = it.toString()
                updateOndatabase(taskurl)
            }

        }.addOnFailureListener {
            toast(it.localizedMessage)
        }
    }

    private fun updateOndatabase(taskurl: String) {
        var map = HashMap<String, String>()
        map.put("image", taskurl)
        firestore.changeProfilePic(user?.uid!!, map).addOnFailureListener {
            toast(it.message!!)
        }.addOnCompleteListener {
            if (it.isSuccessful) {
                hideProgress()
                toast("uploaded successful")
                user.image = taskurl
                setprefObject("user", user)
                Glide.with(context!!).load(path).into(profile_image)
            }
        }

    }
}


