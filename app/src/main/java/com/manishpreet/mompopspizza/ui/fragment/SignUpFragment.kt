package com.manishpreet.mompopspizza.ui

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.Query
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.User
import com.manishpreet.mompopspizza.networking.FirestoreHelper
import com.manishpreet.mompopspizza.utils.*
import kotlinx.android.synthetic.main.fragment_signup.*

class SignUpFragment:Fragment() {

    private val firestoreHelper by lazy { FirestoreHelper.getInstance() }
    lateinit var interactor : HomeInteractor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_signup)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        text_signup_login.onClick {
            interactor.replaceFragment(SignInFragment())
        }
        signup_button.onClick {
            doRegister()
        }

    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        interactor=activity as SignInActivity
    }

    private fun doRegister() {
        var name=signup_edit_name.text.toString().trim()
        var email=signup_edit_email.text.toString().trim()
        var mobile=signup_edit_mobile.text.toString().trim()
        var password=signup_edit_password.text.toString().trim()
        if (name.isEmpty() || email.isEmpty() || mobile.isEmpty() ||password.isEmpty())
            toast("all field required")
        else {
            var user=User(name=name,mobile = mobile,email = email,password = password,image = "https://manjeettattooz.com/wp-content/uploads/2018/09/User-dummy-300x300.png")
            registerDoing(user)
        }
    }

    private fun registerDoing(user: User) {
        context?.showProgress()
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(user.email!!,user.password!!)
            .addOnFailureListener {
                hideProgress()
                toast(it.message!!)
            }.addOnCompleteListener {
                if (it.isSuccessful)
                {
                    var firebaseUser=it.result?.user
                    user.uid=firebaseUser?.uid
                    saveToDatabase(user)
                }
            }
    }

    private fun saveToDatabase(user: User) {
        var query = firestoreHelper.saveUser(user = user,uid = user.uid!!)
        query.addOnCompleteListener {
            setprefObject("user",user)
            hideProgress()
            activity?.launchActivity<LocationActivity>()
        }.addOnFailureListener {
            toast(it.message!!)
        }

    }
}