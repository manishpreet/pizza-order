package com.manishpreet.mompopspizza.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.Pizza
import com.manishpreet.mompopspizza.ui.HomeActivity
import com.manishpreet.mompopspizza.ui.adapter.*
import com.manishpreet.mompopspizza.utils.Constants
import com.manishpreet.mompopspizza.utils.getprefObject
import com.manishpreet.mompopspizza.utils.launchActivity
import com.manishpreet.mompopspizza.utils.onClick
import kotlinx.android.synthetic.main.activity_product.*

class ProductActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)
        setSupportActionBar(toolbar)
        toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_back)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        val products = getprefObject<Pizza>("pizza")

        when {
            Constants.SELECTEDPRODUCT == "chicken" -> {
                supportActionBar?.title = "Chicken"
                val list = products?.itemsArray?.chickenWings
                product_recycler.adapter = ChickenAdapter(list, this)
            }
            Constants.SELECTEDPRODUCT == "fries" -> {
                supportActionBar?.title = "French Fries"
                val list = products?.itemsArray?.frenchFries
                product_recycler.adapter = FrenchFryAdapter(list, this)
            }
            Constants.SELECTEDPRODUCT == "pasta" -> {
                supportActionBar?.title = "Pasta"
                val list = products?.itemsArray?.pasta
                product_recycler.adapter = PastaAdapter(list, this)
            }
            Constants.SELECTEDPRODUCT == "soda" -> {
                supportActionBar?.title = "Soda Pops"
                val list = products?.itemsArray?.sodaPop
                product_recycler.adapter = SodaAdapter(list, this)
            }
            Constants.SELECTEDPRODUCT == "calzones" -> {
                supportActionBar?.title = "Calzones"
                val list = products?.itemsArray?.calzones
                product_recycler.adapter = CalzonesAdapter(list, this)
            }
            Constants.SELECTEDPRODUCT == "pizza" -> {
                supportActionBar?.title = "Pizza"
                val list = products?.itemsArray?.pizzas
                product_recycler.adapter = PizzaAdapter(list, this)
            }
        }
        product_add.onClick {
            launchActivity<HomeActivity>()
            {
                putExtra("key","cart")
            }
        }

    }

}