package com.manishpreet.mompopspizza.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.RealCart
import com.manishpreet.mompopspizza.utils.inflate
import com.sachtechsolution.friendfinder.ui.home.contacts.model.Card
import kotlinx.android.synthetic.main.item_payment.view.*

class PaymentAdapter(val list: MutableList<RealCart>?) : RecyclerView.Adapter<PaymentAdapter.MyPaymentAdapter>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyPaymentAdapter {
        val view = p0?.inflate(R.layout.item_payment)
        return MyPaymentAdapter(view)
    }

    override fun getItemCount(): Int {
        return list?.size!!
    }

    override fun onBindViewHolder(p0: MyPaymentAdapter, p1: Int) {
        p0.itemView.item_count_payment.text = "${list?.get(p1)?.count.toString()}x"
        p0.itemView.item_name_payment.text = list?.get(p1)?.name
        p0.itemView.item_price_payment.text = list?.get(p1)?.price

    }




    inner class MyPaymentAdapter(view: View) : RecyclerView.ViewHolder(view)
}

interface getTotalPrice {
    fun getTotal(total: Int)
}