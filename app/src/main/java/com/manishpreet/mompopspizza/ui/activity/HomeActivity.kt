package com.manishpreet.mompopspizza.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.manishpreet.mompopspizza.*
import com.manishpreet.mompopspizza.ui.fragment.CartFragment
import com.manishpreet.mompopspizza.ui.fragment.ProfileFragment
import com.manishpreet.mompopspizza.utils.addFragment
import com.manishpreet.mompopspizza.utils.replaceFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    val container=R.id.container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        if (intent.getStringExtra("key")!=null) {
            home_navigation.selectedItemId = R.id.card
            addFragment(container, CartFragment())
        }
        else
            addFragment(container, HomeFragment())


        home_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    replaceFragment(container, HomeFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.order -> {
                    replaceFragment(container, OrderFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.profile -> {
                    replaceFragment(container, ProfileFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.card->{
                    replaceFragment(container, CartFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                else -> return@setOnNavigationItemSelectedListener false
            }

        }
    }


}
