package com.manishpreet.mompopspizza.networking

import com.manishpreet.mompopspizza.data.Pizza
import io.reactivex.Single
import retrofit2.http.GET

interface ApiInterface {
    @GET("product/read")
   fun getProducts():Single<Pizza>
}