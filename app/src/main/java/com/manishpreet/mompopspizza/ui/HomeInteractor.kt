package com.manishpreet.mompopspizza.ui

import android.support.v4.app.Fragment

interface HomeInteractor
{
     fun addFragment(fragment : Fragment)
     fun replaceFragment(fragment : Fragment)
}