package com.manishpreet.mompopspizza.ui.dialog

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.utils.*
import kotlinx.android.synthetic.main.dialog_payment.*
import kotlinx.android.synthetic.main.dialog_payment.view.*

class PaymentDialog : DialogFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.dialog_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        get("kvlfv", true)

        dialog_payment_cancel.onClick { dismiss() }
        view.dialog_payment_ok.onClick {
            if (payment_cod.isChecked) {
                SuccessDialog().show(fragmentManager, PaymentDialog::class.java.name)
            }

            else if (payment_credit.isChecked)
                CreditCardDialog().show(fragmentManager, PaymentDialog::class.java.name)
        }

    }

}
