package com.manishpreet.mompopspizza.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.ChickenWing
import com.manishpreet.mompopspizza.ui.activity.ProductActivity
import com.manishpreet.mompopspizza.utils.*
import com.sachtechsolution.friendfinder.extension.saveCard
import com.sachtechsolution.friendfinder.ui.home.contacts.model.Card
import kotlinx.android.synthetic.main.item_product.view.*

class ChickenAdapter(
    val list: List<ChickenWing>?,
    val context: ProductActivity
) : RecyclerView.Adapter<ChickenAdapter.MyChickenAdapter>() {

    fun generateRandomString(len: Int = 8): String{
        val alphanumerics = CharArray(26) { it -> (it + 97).toChar() }.toSet()
            .union(CharArray(9) { it -> (it + 48).toChar() }.toSet())
        return (0..len-1).map {
            alphanumerics.toList().random()
        }.joinToString("")
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyChickenAdapter {
        return MyChickenAdapter(p0?.inflate(R.layout.item_product))
    }

    override fun getItemCount(): Int {
        return list?.size!!
    }

    override fun onBindViewHolder(holder: MyChickenAdapter, i: Int) {
        var chicken=list?.get(i)
        holder.itemView.product_name.text= chicken?.name
        holder.itemView.product_price.text="Price : $${chicken?.price}"
        val url="${chicken?.baseUrl}${chicken?.image}"
        Glide.with(context).asBitmap().load(url).into(holder.itemView.product_image)
        holder.itemView.product_add.also {

            it.onClick {
                if (it.text.toString().equals("Add "))
                {
                    var card = Card(
                        name = chicken?.name,
                        order_number = chicken?.id,
                        price = chicken?.price,
                        count = 1,
                        note = chicken?.type
                    )
                    saveCard(card)
                    toast("added to cart")
                    holder.itemView.product_add.text = "Added"
                }

            }

        }

    }

    inner class MyChickenAdapter(view: View) : RecyclerView.ViewHolder(view)

}
