package com.manishpreet.mompopspizza.ui

import android.app.Activity
import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.google.firebase.auth.FirebaseAuth
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.User
import com.manishpreet.mompopspizza.networking.FirestoreHelper
import com.manishpreet.mompopspizza.utils.*
import kotlinx.android.synthetic.main.fragment_signin.*

class SignInFragment : Fragment() {

    lateinit var interactor: HomeInteractor
    val firestore by lazy { FirestoreHelper.getInstance() }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_signin)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        login_button.onClick {
            doLogin()
        }
        signin_signup_text.onClick {

            interactor.replaceFragment(SignUpFragment())
        }
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        interactor=activity as SignInActivity
    }

    private fun doLogin() {
        var email=login_edit_email.text.toString().trim()
        var password=login_edit_password.text.toString().trim()
        if (email.isEmpty() || password.isEmpty())
            toast("All the field required")
        else
            loginDoing(email,password)

    }

    private fun loginDoing(email: String, password: String) {
        context?.showProgress()
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
            .addOnCompleteListener {
                if (it.isSuccessful)
                {
                    getUser(it.result?.user!!.uid)
                }
            }.addOnFailureListener {
                hideProgress()
                toast(it.message!!)
            }

    }

    private fun getUser(uid: String) {
        firestore.getUser(uid)
            .addOnCompleteListener {
               if (it.isSuccessful)
               {
                   hideProgress()
                   var user=it.result?.toObject(User::class.java)
                   setprefObject("user",user)
                   toast("Login Success")
                   activity?.launchActivity<HomeActivity>()
                   activity?.finish()
               }
            }.addOnFailureListener {
                toast(it.message!!)
                hideProgress()
            }
    }
}
