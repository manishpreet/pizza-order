package com.manishpreet.mompopspizza.data

import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

data class Calzone(

    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("base_url") var baseUrl: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("price") var price: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("unique_id") var uniqueId: String? = null
)

data class User(

    @SerializedName("name") var name: String? = "",
    @SerializedName("mobile") var mobile: String? = "",
    @SerializedName("email") var email: String? = "",
    @SerializedName("password") var password: String? = "",
    @SerializedName("uid") var uid: String? = "",
    @SerializedName("image") var image: String? = "")

   data class RealCart(
    @SerializedName("name") var name: String? = "",
    @SerializedName("note") var note: String? = "",
    @SerializedName("count") var count: Int? = 1,
    @SerializedName("price") var price: String? = "",
    @SerializedName("order_number") var order_number: String? = ""
   )

data class Order(
    @SerializedName("orders") var orders: ArrayList<RealCart>? = null,
    @SerializedName("timestemp") var timestemp: Long? = Date().time,
    @SerializedName("sub_total") var sub_total: String? = "",
    @SerializedName("uid") var uid: String? = "",
    @SerializedName("order_number") var order_number: String? = ""
)


data class ChickenWing(
    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("base_url") var baseUrl: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("price") var price: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("unique_id") var uniqueId: String? = null)


data class FrenchFry (
    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("base_url") var baseUrl: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("price") var price: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("unique_id") var uniqueId: String? = null
)

data class ItemsArray(
    @SerializedName("Pizzas") var pizzas: List<Pizza_>? = null,
    @SerializedName("Calzones") var calzones: List<Calzone>? = null,
    @SerializedName("Pasta") var pasta: List<Pastum>? = null,
    @SerializedName("Chicken Wings") var chickenWings: List<ChickenWing>? = null,
    @SerializedName("French Fries") var frenchFries: List<FrenchFry>? = null,
    @SerializedName("Soda Pop") var sodaPop: List<SodaPop>? = null)


data class Pastum(
    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("base_url") var baseUrl: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("price") var price: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("unique_id") var uniqueId: String? = null)



data class Pizza(
    @SerializedName("items_array") var itemsArray: ItemsArray? = null
)

data class Pizza_(
    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("base_url") var baseUrl: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("price") var price: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("unique_id") var uniqueId: String? = null
)

data class SodaPop(
    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("base_url") var baseUrl: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("price") var price: String? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("unique_id") var uniqueId: String? = null)
