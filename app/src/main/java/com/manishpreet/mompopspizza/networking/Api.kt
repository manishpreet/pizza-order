package com.manishpreet.mompopspizza.networking

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class Api
{ fun createService(): ApiInterface {
    val interceptor = HttpLoggingInterceptor()
    interceptor.level = HttpLoggingInterceptor.Level.BODY
    val client = OkHttpClient.Builder()
        .addInterceptor(interceptor)
    val retrofit = Retrofit.Builder()
        .baseUrl(API_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
        .client(client.build())
        .build()
    return retrofit.create(ApiInterface::class.java)
}

    companion object {
        private const val API_BASE_URL = "http://stsmentor.com/Mom&Pop/"
        val instance by lazy { Api().createService() }
        fun getService() = instance
    }
}

fun apiHitter(): ApiInterface {
    return Api.getService()
}