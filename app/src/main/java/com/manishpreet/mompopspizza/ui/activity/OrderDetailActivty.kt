package com.manishpreet.mompopspizza.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.Order
import com.manishpreet.mompopspizza.ui.adapter.PaymentAdapter
import com.manishpreet.mompopspizza.utils.onClick
import kotlinx.android.synthetic.main.activity_order_detail_activty.*

class OrderDetailActivty : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail_activty)


        var order=Gson().fromJson(intent.getStringExtra("order"),Order::class.java)
        detail_recycler.adapter= PaymentAdapter(order?.orders)
        order_id.text="order id : ${order.order_number}"
        var price=order.sub_total?.split(":")
        payment_view.text="Price : ${price!![1]}"
        payment_count.text="items : ${order.orders?.size.toString()}"
        back_button.onClick {
            onBackPressed()
        }
    }
}
