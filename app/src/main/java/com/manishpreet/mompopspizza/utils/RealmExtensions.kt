package com.sachtechsolution.friendfinder.extension

import com.manishpreet.mompopspizza.utils.RealmController
import com.sachtechsolution.friendfinder.ui.home.contacts.model.Card
import io.realm.Realm





fun saveCard(card: Card) {
    var realm = RealmController().realm
    realm.beginTransaction()
    realm.insertOrUpdate(card)
    realm.commitTransaction()
    realm.close()
}


fun clearRealm() {
    var realm = RealmController().realm
    // obtain the results of a query

    val results = realm.where(Card::class.java).findAll()

    // All changes to data must happen in a transaction
    realm.beginTransaction()

    // Delete all matches
    results.deleteAllFromRealm()
    realm.commitTransaction()
}

fun removeCard(card: Card) {
    var realm = RealmController().realm
    realm.executeTransaction { realm ->
        val rows = realm.where(Card::class.java!!).equalTo("order_number", card.order_number).findAll()
        rows.deleteAllFromRealm()
    }

}

fun updateCard(card: Card) {
    var realm = RealmController().realm
    val toEdit = realm.where(Card::class.java)
        .equalTo("order_number", card.order_number).findFirst()
    realm.beginTransaction()
    toEdit?.price=card.price
    toEdit?.count=card.count
    realm.copyToRealmOrUpdate(card)
    realm.commitTransaction()
}


fun getCards(): MutableList<Card>? {
    var realm = RealmController().realm
    val dd = realm.where(Card::class.java).findAll()
    dd.size
    realm.beginTransaction()
    var list=realm.copyToRealm(dd)
    realm.commitTransaction();
    return list

}
