package com.sachtechsolution.friendfinder.ui.home.contacts.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Card : RealmObject {

    @PrimaryKey
    var order_number: String?=null
    var name: String?=null
    var note: String?=null
    var price: String?=null
    var count: Int?=null


    constructor()

    constructor(name: String? = "" , order_number: String? = "", note: String? = "",price: String? = "", count:Int? = 1) {
        this.name = name
        this.order_number = order_number
        this.note = note
        this.price = price
        this.count=count

    }
}
