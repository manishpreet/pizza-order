package com.manishpreet.mompopspizza.utils

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

fun View.onClick(body: () -> Unit) {
    this.setOnClickListener { body() }
}

fun toast(message: String) {
    Toast.makeText(App.application, message, Toast.LENGTH_SHORT).show()
}

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

inline fun <reified T> Context.launchActivity(body: Intent.() -> Unit = {}) {
    val intent = Intent(this, T::class.java)
    intent.body()
    startActivity(intent)
}

fun AppCompatActivity.addFragment(container: Int, fragment: Fragment) {
    var transaction = supportFragmentManager.beginTransaction()
    transaction.add(container, fragment)
    transaction.commit()
}

fun AppCompatActivity.replaceFragment(container: Int, fragment: Fragment) {
    var transaction = supportFragmentManager.beginTransaction()
    transaction.replace(container, fragment)
    transaction.commit()
}

fun Fragment.replaceChildFragment(container: Int, fragment: Fragment) {
    var transaction = childFragmentManager.beginTransaction()
    transaction.replace(container, fragment).commit()
}

var dialog: ProgressDialog? = null

fun Context.showProgress() {
    if(dialog==null) {
        dialog = ProgressDialog(this)
        dialog?.setMessage("wait a sec")
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.show()
    }
}

fun hideProgress() {
    if (dialog != null)
        dialog?.dismiss()
}
fun View.visible() {
    visibility = View.VISIBLE
}
fun View.gone() {
    visibility = View.GONE
}




