package com.manishpreet.mompopspizza.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.networking.apiHitter
import com.manishpreet.mompopspizza.ui.activity.ProductActivity
import com.manishpreet.mompopspizza.utils.Constants
import com.manishpreet.mompopspizza.utils.inflate
import com.manishpreet.mompopspizza.utils.onClick
import com.manishpreet.mompopspizza.utils.setprefObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_home)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getProductDetails()
        setOnClick()
    }

    private fun setOnClick() {
        home_calzones.onClick {
            Constants.SELECTEDPRODUCT = "calzones"
            openProductActivity()
        }
        home_chicken.onClick {
            Constants.SELECTEDPRODUCT = "chicken"
            openProductActivity()
        }
        home_fries.onClick {
            Constants.SELECTEDPRODUCT = "fries"
            openProductActivity()
        }
        home_pasta.onClick {
            Constants.SELECTEDPRODUCT = "pasta"
            openProductActivity()
        }
        home_pizza.onClick {
            Constants.SELECTEDPRODUCT = "pizza"
            openProductActivity()
        }
        home_soda.onClick {
            Constants.SELECTEDPRODUCT = "soda"
            openProductActivity()
        }
    }

    private fun openProductActivity() {
        context?.startActivity(Intent(context!!, ProductActivity::class.java))
    }

    @SuppressLint("CheckResult")
    private fun getProductDetails() {
        apiHitter().getProducts().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
            {
                setprefObject("pizza", it)
            }
        )
        {

        }
    }
}