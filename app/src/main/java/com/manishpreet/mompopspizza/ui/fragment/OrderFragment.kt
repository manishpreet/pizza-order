package com.manishpreet.mompopspizza.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.Order
import com.manishpreet.mompopspizza.data.User
import com.manishpreet.mompopspizza.networking.FirestoreHelper
import com.manishpreet.mompopspizza.utils.*
import kotlinx.android.synthetic.main.fragment_order.*

class OrderFragment : Fragment(){
    val adapter by lazy { OrderAdapter() }
    val user by lazy { getprefObject<User>("user") }
    val firestoreHelper by lazy { FirestoreHelper.getInstance() }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_order)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        order_recycler_view.adapter=adapter
        getOrders()
    }

    private fun getOrders() {
        firestoreHelper.getPlacedOrder(user.uid!!)
            .addOnCompleteListener {
                if (it.isSuccessful)
                {
                  if (it.result?.size()==0)
                  {
                      emptyView?.visible()
                  }else
                  {
                      emptyView?.gone()
                      it.result?.forEach {
                          var order=it.toObject(Order::class.java)
                          adapter.add(order)
                      }
                  }

                }

            }.addOnFailureListener {
                toast(it.message!!)
            }
    }


}