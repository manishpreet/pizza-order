package com.manishpreet.mompopspizza.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.RealCart
import com.manishpreet.mompopspizza.utils.Constants
import com.manishpreet.mompopspizza.utils.inflate
import com.manishpreet.mompopspizza.utils.onClick
import com.sachtechsolution.friendfinder.extension.removeCard
import com.sachtechsolution.friendfinder.ui.home.contacts.model.Card
import kotlinx.android.synthetic.main.item_cart.view.*

class CartAdapter(
    val real_list: ArrayList<RealCart>,
    var list: MutableList<Card>
) : RecyclerView.Adapter<CartAdapter.MyCartAdapter>() {
    var cardList: MutableList<RealCart>?

    init {

        cardList = real_list
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyCartAdapter {
        return MyCartAdapter(p0?.inflate(R.layout.item_cart))
    }

    override fun getItemCount(): Int {
        return cardList?.size!!
    }

    override fun onBindViewHolder(holder: MyCartAdapter, position: Int) {
        holder.itemView.cart_item_name.text = cardList?.get(position)?.name
        holder?.itemView.cart_item_price.text = cardList?.get(position)?.price
        holder?.itemView.cart_item_count.text = cardList?.get(position)?.count.toString()
        holder?.itemView.cart_item_desc.text = cardList?.get(position)?.note
        holder.itemView.apply {

            card_item_increment.onClick {
                var count=cart_item_count.text.toString().toInt()
                var real_count=count+1
                cart_item_count.text=real_count.toString()
                cardList?.get(position)?.count=real_count


                var price=cart_item_price.text.toString().toInt()
                var real_price=price+ list?.get(position)?.price?.toInt()!!
                cart_item_price.text=real_price.toString()
                cardList?.get(position)?.price=real_price.toString()
            }
            card_item_decrement.onClick {
                var count=cart_item_count.text.toString().toInt()
                if (count>1)
                {
                    var real_count=count-1
                    cart_item_count.text=real_count.toString()
                    cardList?.get(position)?.count=real_count

                    var price=cart_item_price.text.toString().toInt()
                    var real_price=price- list?.get(position)?.price?.toInt()!!
                    cart_item_price.text=real_price.toString()
                    cardList?.get(position)?.price=real_price.toString()


                }
            }
        }

        holder?.itemView.cart_item_remove.onClick {
            removeCard(list?.get(position)!!)
            cardList?.removeAt(position)
            notifyItemRemoved(position)
            Constants.CARTLIST=cardList
        }
    }

    inner class MyCartAdapter(view: View) : RecyclerView.ViewHolder(view)

}
