package com.manishpreet.mompopspizza.ui.dialog

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.utils.onClick
import kotlinx.android.synthetic.main.dialog_credit_card.*
import kotlinx.android.synthetic.main.fragment_cart.*

class CreditCardDialog:DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_credit_card,container,false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_FRAME,R.style.AppTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       /* card_pay.onClick { SuccessDialog().show(fragmentManager,
            CreditCardDialog::class.java.name) }*/
    }
}
