package com.manishpreet.mompopspizza.ui

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.User
import com.manishpreet.mompopspizza.utils.getprefObject
import com.manishpreet.mompopspizza.utils.launchActivity

class SplashActivity : AppCompatActivity() {
    val user by lazy { getprefObject<User>("user") }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val handler = Handler()
        handler.postDelayed({

           if(user==null)
                launchActivity<SignInActivity>()
            else
                launchActivity<HomeActivity>()
            finish()
        }, 3000)
    }
}