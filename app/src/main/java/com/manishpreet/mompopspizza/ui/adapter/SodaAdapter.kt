package com.manishpreet.mompopspizza.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.manishpreet.mompopspizza.R
import com.manishpreet.mompopspizza.data.SodaPop
import com.manishpreet.mompopspizza.ui.dialog.AddItemOptionDialog
import com.manishpreet.mompopspizza.ui.activity.ProductActivity
import com.manishpreet.mompopspizza.utils.inflate
import com.manishpreet.mompopspizza.utils.onClick
import com.manishpreet.mompopspizza.utils.toast
import com.sachtechsolution.friendfinder.extension.saveCard
import com.sachtechsolution.friendfinder.ui.home.contacts.model.Card
import kotlinx.android.synthetic.main.item_product.view.*

class SodaAdapter(
    val list: List<SodaPop>?,
    val context: ProductActivity
) : RecyclerView.Adapter<SodaAdapter.MySodaAdapter>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MySodaAdapter {
        return MySodaAdapter(p0?.inflate(R.layout.item_product))
    }

    override fun getItemCount(): Int {
        return list?.size!!
    }

    override fun onBindViewHolder(holder: MySodaAdapter, i: Int) {
        val sodaPop=list?.get(i)
        holder.itemView.product_name.text = sodaPop?.name
        holder.itemView.product_price.text="Price : $${sodaPop?.price}"
        val url="${sodaPop?.baseUrl}${sodaPop?.image}"
        Glide.with(context).asBitmap().load(url).into(holder.itemView.product_image)
        holder.itemView.product_add.also {

            it.onClick {
                if (it.text.toString().equals("Add "))
                {
                    var card = Card(
                        name = sodaPop?.name,
                        order_number = sodaPop?.id,
                        price = sodaPop?.price,
                        count = 1,
                        note = sodaPop?.type
                    )
                    saveCard(card)
                    toast("added to cart")
                    holder.itemView.product_add.text = "Added"
                }

            }

        }
    }

    inner class MySodaAdapter(view: View) : RecyclerView.ViewHolder(view)
}
